﻿namespace LPCalcGui
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.offerTable = new System.Windows.Forms.DataGridView();
            this.rewardCostTypeCombo = new System.Windows.Forms.ComboBox();
            this.requirementCostTypeCombo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.regionCombo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.goButton = new System.Windows.Forms.Button();
            this.fetchPricesButton = new System.Windows.Forms.Button();
            this.Item = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lpCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iskCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.reqItems = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grossProfit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.totalCost = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.netProfit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lpIsk = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.corpCombo = new System.Windows.Forms.ComboBox();
            this.corpLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.offerTable)).BeginInit();
            this.SuspendLayout();
            // 
            // offerTable
            // 
            this.offerTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.offerTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Item,
            this.Amount,
            this.lpCost,
            this.iskCost,
            this.reqItems,
            this.grossProfit,
            this.totalCost,
            this.netProfit,
            this.lpIsk});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = "--";
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.offerTable.DefaultCellStyle = dataGridViewCellStyle3;
            this.offerTable.Location = new System.Drawing.Point(12, 52);
            this.offerTable.Name = "offerTable";
            this.offerTable.Size = new System.Drawing.Size(1099, 474);
            this.offerTable.TabIndex = 0;
            // 
            // rewardCostTypeCombo
            // 
            this.rewardCostTypeCombo.FormattingEnabled = true;
            this.rewardCostTypeCombo.Location = new System.Drawing.Point(12, 25);
            this.rewardCostTypeCombo.Name = "rewardCostTypeCombo";
            this.rewardCostTypeCombo.Size = new System.Drawing.Size(121, 21);
            this.rewardCostTypeCombo.TabIndex = 1;
            // 
            // requirementCostTypeCombo
            // 
            this.requirementCostTypeCombo.FormattingEnabled = true;
            this.requirementCostTypeCombo.Location = new System.Drawing.Point(139, 25);
            this.requirementCostTypeCombo.Name = "requirementCostTypeCombo";
            this.requirementCostTypeCombo.Size = new System.Drawing.Size(121, 21);
            this.requirementCostTypeCombo.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Rewards";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(136, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Requirements";
            // 
            // regionCombo
            // 
            this.regionCombo.FormattingEnabled = true;
            this.regionCombo.Location = new System.Drawing.Point(267, 25);
            this.regionCombo.Name = "regionCombo";
            this.regionCombo.Size = new System.Drawing.Size(121, 21);
            this.regionCombo.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(267, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Region";
            // 
            // goButton
            // 
            this.goButton.Location = new System.Drawing.Point(395, 25);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(98, 23);
            this.goButton.TabIndex = 7;
            this.goButton.Text = "Refresh Table";
            this.goButton.UseVisualStyleBackColor = true;
            // 
            // fetchPricesButton
            // 
            this.fetchPricesButton.Location = new System.Drawing.Point(500, 25);
            this.fetchPricesButton.Name = "fetchPricesButton";
            this.fetchPricesButton.Size = new System.Drawing.Size(86, 23);
            this.fetchPricesButton.TabIndex = 8;
            this.fetchPricesButton.Text = "Fetch Prices";
            this.fetchPricesButton.UseVisualStyleBackColor = true;
            // 
            // Item
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.NullValue = null;
            this.Item.DefaultCellStyle = dataGridViewCellStyle1;
            this.Item.HeaderText = "Item";
            this.Item.Name = "Item";
            this.Item.Width = 200;
            // 
            // Amount
            // 
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            // 
            // lpCost
            // 
            this.lpCost.HeaderText = "LP Cost";
            this.lpCost.Name = "lpCost";
            // 
            // iskCost
            // 
            this.iskCost.HeaderText = "ISK Cost";
            this.iskCost.Name = "iskCost";
            // 
            // reqItems
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle2.NullValue = null;
            this.reqItems.DefaultCellStyle = dataGridViewCellStyle2;
            this.reqItems.HeaderText = "Required Items";
            this.reqItems.Name = "reqItems";
            this.reqItems.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // grossProfit
            // 
            this.grossProfit.HeaderText = "Gross Profit";
            this.grossProfit.Name = "grossProfit";
            // 
            // totalCost
            // 
            this.totalCost.HeaderText = "Total Cost";
            this.totalCost.Name = "totalCost";
            // 
            // netProfit
            // 
            this.netProfit.HeaderText = "Net Profit";
            this.netProfit.Name = "netProfit";
            // 
            // lpIsk
            // 
            this.lpIsk.HeaderText = "LP / ISK";
            this.lpIsk.Name = "lpIsk";
            // 
            // corpCombo
            // 
            this.corpCombo.FormattingEnabled = true;
            this.corpCombo.Location = new System.Drawing.Point(989, 25);
            this.corpCombo.Name = "corpCombo";
            this.corpCombo.Size = new System.Drawing.Size(121, 21);
            this.corpCombo.TabIndex = 9;
            // 
            // corpLabel
            // 
            this.corpLabel.AutoSize = true;
            this.corpLabel.Location = new System.Drawing.Point(1049, 9);
            this.corpLabel.Name = "corpLabel";
            this.corpLabel.Size = new System.Drawing.Size(61, 13);
            this.corpLabel.TabIndex = 10;
            this.corpLabel.Text = "Corporation";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1123, 538);
            this.Controls.Add(this.corpLabel);
            this.Controls.Add(this.corpCombo);
            this.Controls.Add(this.fetchPricesButton);
            this.Controls.Add(this.goButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.regionCombo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.requirementCostTypeCombo);
            this.Controls.Add(this.rewardCostTypeCombo);
            this.Controls.Add(this.offerTable);
            this.Name = "Form1";
            this.Text = "LP Calc";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.offerTable)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView offerTable;
        private System.Windows.Forms.ComboBox rewardCostTypeCombo;
        private System.Windows.Forms.ComboBox requirementCostTypeCombo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox regionCombo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button goButton;
        private System.Windows.Forms.Button fetchPricesButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn Item;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn lpCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn iskCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn reqItems;
        private System.Windows.Forms.DataGridViewTextBoxColumn grossProfit;
        private System.Windows.Forms.DataGridViewTextBoxColumn totalCost;
        private System.Windows.Forms.DataGridViewTextBoxColumn netProfit;
        private System.Windows.Forms.DataGridViewTextBoxColumn lpIsk;
        private System.Windows.Forms.ComboBox corpCombo;
        private System.Windows.Forms.Label corpLabel;
    }
}

