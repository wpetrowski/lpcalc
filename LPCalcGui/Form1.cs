﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LPCalcCore;

namespace LPCalcGui
{
    public partial class Form1 : Form
    {
        EveDB eveDB = new EveDB();
        IEnumerable<StoreOffer> offers;
        IEnumerable<Corporation> corps;

        CostType[] costTypes = new CostType[] { CostType.BuyMax, CostType.SellMin };

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            offers = Core.GetAllOffers(eveDB);
            corps = Core.GetUniqueCorps(offers);

            setCostTypesIntoCombo(rewardCostTypeCombo);
            setCostTypesIntoCombo(requirementCostTypeCombo);
            setCorporationsIntoCombo(corpCombo);

            foreach (StoreOffer o in offers)
            {
                addOffer(o);
            }
        }


        private void setCostTypesIntoCombo(ComboBox combo)
        {
            combo.DataSource = costTypes;
        }

        private void setCorporationsIntoCombo(ComboBox combo)
        {
            combo.DataSource = corps.ToList();
        }

        private void addOffer(StoreOffer o)
        {
            offerTable.Rows.Add();
            DataGridViewRow row = offerTable.Rows[offerTable.Rows.Count - 2];

            row.Cells["Item"].Value = o.Item.ItemName;
            row.Cells["Amount"].Value = o.NumberOffered.ToString();
            row.Cells["lpCost"].Value = o.LPCost;
            row.Cells["iskCost"].Value = o.IskCost;
            row.Cells["reqItems"].Value = String.Join("\n", o.ItemRequirements.Select(x => x.Item1.ItemName));
            row.Cells["grossProfit"].Value = o.GetGrossProfit(CostType.BuyMax);
            row.Cells["totalCost"].Value = o.GetTotalIskCost(CostType.SellMin);
            row.Cells["netProfit"].Value = o.GetNetProfit(CostType.BuyMax, CostType.SellMin);
            row.Cells["lpIsk"].Value = o.GetIskPerLP(CostType.BuyMax, CostType.SellMin);

        }

    }
}
