﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LPCalcCore;

namespace LPCalcCommand
{
    class Program
    {
        static EveDB eveDB = new EveDB();

        static List<Item> items;

        static string filename;
        static string region;

        static void Main(string[] args)
        {
            items = new List<Item>();
            processArguments(args);
        }

        private static void processArguments(string[] args)
        {
            Action action = listOffers;

            foreach (String arg in args)
            {
                if (arg.Equals("/create_offers", StringComparison.OrdinalIgnoreCase))
                {
                    action = createOffers;
                }
                if (arg.StartsWith("/filename", StringComparison.OrdinalIgnoreCase))
                {
                    parseFilename(arg);
                }
                if (arg.StartsWith("/region", StringComparison.OrdinalIgnoreCase))
                {
                    parseRegion(arg);
                }
            }

            action.Invoke();
        }

        private static void parseFilename(string arg)
        {
 	        string[] parts = arg.Split('=');
            filename = parts[1];
        }

        private static void parseRegion(string arg)
        {
            string[] parts = arg.Split('=');
            region = parts[1];
            region = region.Trim('\'', '"');
        }

        private static void listOffers()
        {
            IEnumerable<StoreOffer> offers = Core.GetTopOffers(eveDB, 10, CostType.BuyMax, CostType.SellMin, region);

            foreach (StoreOffer o in offers)
            {
                listOffer(o);
                writeColoredText(ConsoleColor.Magenta, "ISK/LP: {0}", Math.Round(o.GetIskPerLP(CostType.BuyMax, CostType.SellMin), 2));
                Console.WriteLine();
                Console.WriteLine();
            }
        }

        private static void listOffer(StoreOffer offer)
        {
            writeColoredText(ConsoleColor.Cyan, "{0}", offer.Item.ItemName);

            if (offer.NumberOffered > 1)
            {
                writeColoredText(ConsoleColor.Green, " (x{0})", offer.NumberOffered);
            }

            Console.WriteLine();

            writeColoredText(ConsoleColor.White, "   ISK: {0,12:N0}\n", offer.IskCost);
            writeColoredText(ConsoleColor.White, "   LP:  {0,12:N0}\n", offer.LPCost);

            if (offer.ItemRequirements.Count > 0)
            {
                writeColoredText(ConsoleColor.Gray, "   The following items are required for this offer:\n");
                foreach (Tuple<Item, int> t in offer.ItemRequirements)
                {
                    writeColoredText(ConsoleColor.DarkCyan, "      {0}", t.Item1.ItemName);
                    if (t.Item2 > 0)
                    {
                        writeColoredText(ConsoleColor.DarkGreen, " (x{0})", t.Item2);
                    }
                    Console.WriteLine();
                }
            }
        }

        private static void createOffers()
        {
            if (filename == null)
                return;

            string text = File.ReadAllText(filename);
            Scraper.AddStoreOffersFromCsv(eveDB, text);
        }

        private static void writeColoredText(ConsoleColor consoleColor, string formatText, params object[] rest)
        {
            Console.ForegroundColor = consoleColor;
            Console.Write(formatText, rest);
            Console.ResetColor();
        }
    }
}
