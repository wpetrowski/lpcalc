﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPCalcCore
{
    public static class Core
    {
        private static List<Item> collectItemsFromOffers(List<StoreOffer> offers)
        {
            Dictionary<int, Item> items = new Dictionary<int, Item>();

            foreach (StoreOffer offer in offers)
            {
                if (!items.ContainsKey(offer.ItemId))
                {
                    items.Add(offer.ItemId, offer.Item);
                }

                foreach (Item i in offer.ItemRequirements.Select(x => x.Item1))
                {
                    if (!items.ContainsKey(i.ItemId))
                    {
                        items.Add(i.ItemId, i);
                    }
                }
            }

            return items.Values.ToList();
        }

        private static void updateAndCacheAllCostsIfNecessary(EveDB eveDB, bool refresh, string region)
        {
            List<StoreOffer> offers = eveDB.getStoreOffers();

            List<Item> allItems = collectItemsFromOffers(offers);

            eveDB.getCosts(allItems);

            int regionId = eveDB.getRegionIdFromName(region);

            if (refresh)
            {
                CostRetriever.GetCosts(allItems, regionId);
                eveDB.storeCosts(allItems);
            }
            else
            {
                DateTime now = DateTime.Now;
                TimeSpan maxAllowedLength = new TimeSpan(1, 0, 0, 0);

                List<Item> staleItems = allItems.Where(x =>
                    {
                        if (x.Cost == null)
                            return true;

                        TimeSpan diff = now.Subtract(x.Cost.RetrievedTime);
                        return diff > maxAllowedLength;
                    }).ToList();

                CostRetriever.GetCosts(staleItems, regionId);
                eveDB.storeCosts(staleItems);
            }
        }

        public static IEnumerable<StoreOffer> GetTopOffers(EveDB db, int howMany, CostType itemCostType, CostType requirementCostType, string region)
        {
            updateAndCacheAllCostsIfNecessary(db, region != null, region);

            List<StoreOffer> offers = db.getStoreOffers();

            return offers.OrderByDescending(x => x.GetIskPerLP(itemCostType, requirementCostType))
                         .Take(howMany);
        }

        public static IEnumerable<StoreOffer> GetAllOffers(EveDB db)
        {
            updateAndCacheAllCostsIfNecessary(db, false, null);
            return db.getStoreOffers();
        }

        public static IEnumerable<Corporation> GetUniqueCorps(IEnumerable<StoreOffer> offers)
        {
            return offers.Select(x => x.Corp).Distinct();
        }
    }
}
