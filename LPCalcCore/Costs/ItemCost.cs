﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace LPCalcCore
{
    public class ItemCost
    {
        private XElement m_data;
        private DateTime m_time;

        public ItemCost(XElement data, DateTime time)
        {
            m_data = data;
            m_time = time;
        }

        // TODO: handle exception
        public double GetCost(CostType costType)
        {
            return Double.Parse(m_data.XPathSelectElement(costType.Selector).Value, CultureInfo.InvariantCulture);
        }

        public string StringData
        {
            get { return m_data.ToString(SaveOptions.DisableFormatting); }
        }

        public DateTime RetrievedTime
        {
            get { return m_time; }
        }
    }
}
