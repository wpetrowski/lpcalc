﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LPCalcCore
{
    public static class CostRetriever
    {
        private static void getCostsHelper(List<Item> items, int regionId)
        {
            string urlTemplate = "http://api.eve-central.com/api/marketstat?{0}";

            if (regionId > 0)
            {
                urlTemplate += "&regionlimit=" + regionId;
            }

            string itemTemplate = "typeid={0}";

            IEnumerable<Item> tempItems = items.Where(x => x.MarketGroupId != int.MinValue);

            // TODO: Investigate whether the market group ID filtering actually works
            string allItems = tempItems.Aggregate("", (v, i) => v + "&" + string.Format(CultureInfo.InvariantCulture, itemTemplate, i.ItemId));

            string url = String.Format(CultureInfo.InvariantCulture, urlTemplate, allItems);
            XDocument doc = XDocument.Load(url);
          
            foreach (Item i in items)
            {
                i.Cost = new ItemCost(doc.Descendants("type").Single(x => (int)x.Attribute("id") == i.ItemId), DateTime.Now);
            }
        }

        //TODO: verify refactoring didn't break this
        public static void GetCosts(IEnumerable<Item> items, int regionId)
        {
            int partitionSize = 20;

            List<Item> tempItems = items.ToList();

            while (tempItems.Count > 0)
            {
                List<Item> currentSet = items.Take(partitionSize).ToList();
                getCostsHelper(currentSet, regionId);

                tempItems = tempItems.Skip(partitionSize).ToList();
            }
        }
    }
}
