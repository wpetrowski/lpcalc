﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LPCalcCore
{
    public class CostType
    {
        private string selector;
        public string Selector
        {
            get { return selector; }
        }

        private CostType(string selector)
        {
            this.selector = selector;
        }

        public static readonly CostType BuyAverage = new CostType("buy/avg");
        public static readonly CostType BuyMax = new CostType("buy/max");
        public static readonly CostType BuyMin = new CostType("buy/min");
        public static readonly CostType BuyMedian = new CostType("buy/median");
        public static readonly CostType SellAverage = new CostType("sell/avg");
        public static readonly CostType SellMax = new CostType("sell/max");
        public static readonly CostType SellMin = new CostType("sell/min");
        public static readonly CostType SellMedian = new CostType("sell/median");

        public override string ToString()
        {
            return Selector;
        }
    }
}
