﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPCalcCore
{
    public static class ItemFactory
    {
        private static List<Item> itemCache = new List<Item>();

        public static Item GetOrCreateItem(int id, string name, int marketId)
        {
            Item i = itemCache.SingleOrDefault(x => x.ItemId == id);

            if (i == null)
            {
                i = new Item(id, name, marketId);
                itemCache.Add(i);
            }

            return i;
        }

    }
}
