﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace LPCalcCore
{
    public class EveDB
    {
        SqlConnection conn;
        List<StoreOffer> cachedStoreOffers;

        public EveDB(string connectionString="")
        {
            if (connectionString == "")
            {
                connectionString = Properties.Settings.Default.ebs_DATADUMPConnectionString;
            }

            conn = new SqlConnection(connectionString);
            conn.Open();
        }

        ~EveDB()
        {
            
        }

        public List<StoreOffer> getStoreOffers(bool refresh = false)
        {
            if (cachedStoreOffers == null || refresh)
            {
                cachedStoreOffers = new List<StoreOffer>();

                SqlCommand cmd = new SqlCommand("SELECT * FROM [dbo].[StoreOffers]", conn);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        StoreOffer so = constructStoreOffer(reader);
                        cachedStoreOffers.Add(so);
                    }
                }

                foreach (StoreOffer offer in cachedStoreOffers)
                {
                    initStoreOffer(offer);
                }
            }

            return cachedStoreOffers;
        }

        private StoreOffer constructStoreOffer(SqlDataReader reader)
        {
            StoreOffer so = new StoreOffer();

            so.OfferId = (int)reader["offerId"];
            so.ItemId = (int)reader["itemId"];
            so.CorpId = (int)reader["corporationId"];
            so.IskCost = (int)reader["iskCost"];
            so.LPCost = (int)reader["lpCost"];
            so.NumberOffered = (int)reader["numOffered"];
            return so;
        }

        private void initStoreOffer(StoreOffer so)
        {
            so.ItemRequirements = getOfferItemRequirements(so.OfferId);

            so.Item = getItem(so.ItemId);
            so.Corp = getCorporation(so.CorpId);
        }

        public List<Tuple<Item, int>> getOfferItemRequirements(int offerId)
        {
            List<Tuple<Item, int>> results = new List<Tuple<Item, int>>();

            string query = "SELECT so.offerId, so.itemId AS typeID, so.amount, it.typeName, it.marketGroupID " +
                           "FROM [StoreOfferItemRequirements] so INNER JOIN [invTypes] it " +
                           "ON so.itemId=it.typeID " +
                           "WHERE so.offerId = " + offerId;
            SqlCommand cmd = new SqlCommand(query, conn);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    int amount = (int)reader["amount"];

                    Item item = initItem(reader);

                    Tuple<Item, int> pair = new Tuple<Item, int>(item, amount);

                    results.Add(pair);
                }
            }

            return results;
        }

        private static Item initItem(SqlDataReader reader)
        {
            int itemID = (int)reader["typeID"];
            string name = (string)reader["typeName"];
            int marketId = int.MinValue;

            object mId = reader["marketGroupID"];
            if (!(mId is System.DBNull))
            {
                marketId = (int)mId;
            }

            return ItemFactory.GetOrCreateItem(itemID, name, marketId);
        }

        private Item getItem(int itemId)
        {
            Item result = null;

            string query = "SELECT * FROM [dbo].[invTypes] WHERE typeID = " + itemId;
            SqlCommand cmd = new SqlCommand(query, conn);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result = initItem(reader);
                }
            }

            if (result.ItemName.Contains("Blueprint"))
            {
                result.IsBlueprint = true;
                string n = result.ItemName.Replace("Blueprint", "").Trim();
                result.BlueprintItem = getItem(n);
            }

            return result;
        }

        public Item getItem(string name)
        {
            Item result = null;

            name = name.Replace("'", "''");

            string query = "SELECT * FROM [dbo].[invTypes] WHERE typeName = '" + name + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result = initItem(reader);              
                }
            }

            if (result.ItemName.Contains("Blueprint"))
            {
                result.IsBlueprint = true;
                string n = result.ItemName.Replace("Blueprint", "").Trim();
                result.BlueprintItem = getItem(n);
            }

            return result;
        }

        private Corporation getCorporation(int corpId)
        {
            Corporation result = new Corporation();
            result.CorpId = corpId;

            string query = "SELECT * FROM [dbo].[invUniqueNames] WHERE itemID = " + corpId;
            SqlCommand cmd = new SqlCommand(query, conn);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.CorpName = (string)reader["itemName"];
                }
            }

            return result;
        }

        public Corporation getCorporation(string name)
        {
            Corporation result = new Corporation();

            name = name.Replace("'", "''");

            string query = "SELECT * FROM [dbo].[invUniqueNames] WHERE itemName = '" + name + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    result.CorpId = (int)reader["itemID"];
                    result.CorpName = (string)reader["itemName"];
                }
            }

            return result;

        }

        public void createOffer(StoreOffer so)
        {
            int maxId = 0;

            string query = "SELECT max(offerId) as maxid FROM [dbo].[StoreOffers]";
            SqlCommand cmd = new SqlCommand(query, conn);
            object result = cmd.ExecuteScalar();
            if (!(result is System.DBNull))
            {
                maxId = (int)result;
            }

            query = "INSERT INTO [dbo].[StoreOffers] (offerId, itemId, corporationId, iskCost, lpCost, numOffered) VALUES ({0}, {1}, {2}, {3}, {4}, {5})";
            query = String.Format(query, maxId + 1, so.ItemId, so.CorpId, so.IskCost, so.LPCost, so.NumberOffered);
            cmd = new SqlCommand(query, conn);
            cmd.ExecuteScalar();

            foreach (Tuple<Item, int> req in so.ItemRequirements)
            {
                query = "INSERT INTO [dbo].[StoreOfferItemRequirements] (offerId, itemId, amount) VALUES ({0}, {1}, {2})";
                query = String.Format(query, maxId + 1, req.Item1.ItemId, req.Item2);
                cmd = new SqlCommand(query, conn);
                cmd.ExecuteScalar();
            }
        }

        public void createOffers(List<StoreOffer> offers)
        {
            foreach (StoreOffer offer in offers)
            {
                createOffer(offer);
            }
        }

        public void storeCosts(List<Item> items)
        {
            foreach (Item i in items)
            {
                if (i.Cost != null)
                {
                    string clear = "DELETE FROM [dbo].[ItemCosts] WHERE itemId = " + i.ItemId;
                    SqlCommand cmd2 = new SqlCommand(clear, conn);
                    cmd2.ExecuteNonQuery();

                    string query = "INSERT INTO [dbo].[ItemCosts] (itemId, cost, time) VALUES (@id, @cost, @time)";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = i.ItemId;
                    cmd.Parameters.Add("@cost", SqlDbType.NVarChar).Value = i.Cost.StringData;
                    cmd.Parameters.Add("@time", SqlDbType.DateTime2, 8).Value = i.Cost.RetrievedTime;

                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void getCosts(List<Item> items)
        {
            foreach (Item i in items)
            {
                string query = "SELECT * FROM [dbo].[ItemCosts] WHERE itemId = " + i.ItemId;
                SqlCommand cmd = new SqlCommand(query, conn);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string costs = (string)reader["cost"];
                        DateTime time = (DateTime)reader["time"];
                        XElement xml = XElement.Parse(costs);
                        i.Cost = new ItemCost(xml, time);
                    }
                }
            }
        }

        public int getRegionIdFromName(string name)
        {
            int id = int.MinValue;

            if (name != null)
            {
                name = name.Replace("'", "''");

                string query = "SELECT regionID FROM [dbo].[mapRegions] WHERE regionName = '" + name + "'";
                SqlCommand cmd = new SqlCommand(query, conn);
                object result = cmd.ExecuteScalar();
                if (!(result is System.DBNull))
                {
                    id = (int)result;
                }
            }

            return id;
        }

    }
}
