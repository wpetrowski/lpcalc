﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LPCalcCore
{
    public static class Scraper
    {
        public static void AddStoreOffersFromCsv(EveDB eveDB, string text)
        {
            List<StoreOffer> offersToAdd = new List<StoreOffer>();
            List<string> errors = new List<string>();

            StringReader sr = new StringReader(text);

            string line = sr.ReadLine();

            while (line != null)
            {
                bool error = false;

                StoreOffer so = new StoreOffer();

                string[] parts = line.Split(',');

                string corpName = parts[0];
                string rewardItem = parts[1];
                int lpCost = int.Parse(parts[2], CultureInfo.InvariantCulture);
                int iskCost = int.Parse(parts[3], CultureInfo.InvariantCulture);
                string requiredItem = parts[4];

                so.Corp = eveDB.getCorporation(corpName);
                so.CorpId = so.Corp.CorpId;

                Tuple<Item, int> reward = parseItemAndCount(eveDB, rewardItem);
                so.ItemId = reward.Item1.ItemId;
                so.NumberOffered = reward.Item2;
                so.Item = reward.Item1;

                if (so.ItemId == 0)
                {
                    error = true;
                    errors.Add(line);
                }

                so.LPCost = lpCost;

                so.IskCost = iskCost;

                if (requiredItem.Length == 0)
                {
                    Tuple<Item, int> ii = parseItemAndCount(eveDB, requiredItem);
                    so.ItemRequirements.Add(ii);

                    if (ii.Item1.ItemId == 0)
                    {
                        error = true;
                        errors.Add(line);
                    }
                }

                line = sr.ReadLine();

                while (line != null && line.StartsWith(",,,,", StringComparison.OrdinalIgnoreCase))
                {
                    Tuple<Item, int> ii = parseItemAndCount(eveDB, line.Substring(4));
                    so.ItemRequirements.Add(ii);
                    line = sr.ReadLine();

                    if (ii.Item1.ItemId == 0)
                    {
                        error = true;
                        errors.Add(line);
                    }
                }

                if (!error)
                    offersToAdd.Add(so);
            }

            if (errors.Count == 0)
            {
                eveDB.createOffers(offersToAdd);
            }
            else
            {
                Console.WriteLine("There were errors:");
                foreach (string so in errors)
                {
                    Console.WriteLine("{0}", so);
                }
            }
        }

        private static Tuple<Item, int> parseItemAndCount(EveDB eveDB, string rewardItem)
        {
            string[] parts = rewardItem.Split(new string[] { " x " }, StringSplitOptions.None);
            int count = int.Parse(parts[0], CultureInfo.InvariantCulture);
            string itemName = parts[1];

            Item i = eveDB.getItem(itemName);
            return new Tuple<Item, int>(i, count);
        }

    }
}
