﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LPCalcCore
{
    public class Corporation
    {
        private int corpId;
        public int CorpId
        {
            get { return corpId; }
            set { corpId = value; }
        }

        private string corpName;
        public string CorpName
        {
            get { return corpName; }
            set { corpName = value; }
        }

        public override string ToString()
        {
            return CorpName;
        }

        public override bool Equals(object obj)
        {
            Corporation other = obj as Corporation;

            if (other != null)
            {
                return CorpId == other.CorpId;
            }

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return CorpId.GetHashCode();
        }
    }
}
