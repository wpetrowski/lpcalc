﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LPCalcCore
{
    public class StoreOffer
    {
        public int OfferId
        {
            get; set;
        }

        public int ItemId
        {
            get; set;
        }

        public int CorpId
        {
            get; set;
        }

        public int IskCost
        {
            get; set;
        }

        public int LPCost
        {
            get; set;
        }

        public int NumberOffered
        {
            get; set;
        }

        private IList<Tuple<Item, int>> itemRequirements;
        public IList<Tuple<Item, int>> ItemRequirements
        {
            get { return itemRequirements; }
            set { itemRequirements = value; }
        }

        private Item item;
        public Item Item
        {
            get { return item; }
            set { item = value; }
        }

        private Corporation corp;
        public Corporation Corp
        {
            get { return corp; }
            set { corp = value; }
        }

        public StoreOffer()
        {
            itemRequirements = new List<Tuple<Item, int>>();
        }

        public double GetGrossProfit(CostType costType)
        {
            return NumberOffered * Item.GetManufacturedCost(costType);
        }
        
        public double GetTotalIskCost(CostType costType)
        {
            return ItemRequirements.Aggregate((double)IskCost, (currCost, t) => currCost + (t.Item1.GetManufacturedCost(costType) * t.Item2));
        }

        public double GetNetProfit(CostType itemCostType, CostType requirementsCostType)
        {
            return GetGrossProfit(itemCostType) - GetTotalIskCost(requirementsCostType);
        }

        public double GetIskPerLP(CostType itemCostType, CostType requirementsCostType)
        {
            return GetNetProfit(itemCostType, requirementsCostType) / LPCost;
        }
    }
}
