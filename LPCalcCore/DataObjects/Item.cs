﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LPCalcCore
{
    public class Item
    {
        private int itemId;
        public int ItemId
        {
            get { return itemId; }
        }

        private string itemName;
        public string ItemName
        {
            get { return itemName; }
        }
        
        private int marketGroupId;
        public int MarketGroupId
        {
            get { return marketGroupId; }
        }

        private bool isBlueprint;
        public bool IsBlueprint
        {
            get { return isBlueprint; }
            set { isBlueprint = value; }
        }

        private Item blueprintItem;
        public Item BlueprintItem
        {
            get { return blueprintItem; }
            set { blueprintItem = value; }
        }

        private ItemCost cost;
        public ItemCost Cost
        {
            get { return cost; }
            set { cost = value; }
        }

        public Item(int id, string name, int marketId)
        {
            itemId = id;
            itemName = name;
            marketGroupId = marketId;
            isBlueprint = false;
            blueprintItem = null;
        }

        public double GetManufacturedCost(CostType costType)
        {
            if (IsBlueprint)
            {
                return BlueprintItem.Cost.GetCost(costType);
            }
            else
            {
                return Cost.GetCost(costType);
            }
        }
    }
}
